package com.example.joyce.cadastrocurso;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import static com.example.joyce.cadastrocurso.CadastroActivity.lista;

public class ListaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_views);

        List<Curso> cursos = lista;
        ListView listaview = (ListView) findViewById(R.id.lista_curso);

        final ArrayAdapter<Curso> adapter = new ArrayAdapter<Curso>(this, android.R.layout.simple_list_item_1, cursos);
        listaview.setAdapter(adapter);

        listaview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Activity2.class);

                Curso c = adapter.getItem(position);

                String txt = c.getNome();
                Bundle bundle = new Bundle();
                bundle.putString("txt", txt);
                intent.putExtras(bundle);

                String txt2 = c.getDescricao();
                Bundle bundle2 = new Bundle();
                bundle2.putString("txt2", txt2);
                intent.putExtras(bundle2);

                String txt3 = c.getProfessor();
                Bundle bundle3 = new Bundle();
                bundle3.putString("txt3", txt3);
                intent.putExtras(bundle3);

                String txt4 = c.getTipo();
                Bundle bundle4 = new Bundle();
                bundle4.putString("txt4", txt4);
                intent.putExtras(bundle4);

                String txt5 = c.getInicio();
                Bundle bundle5 = new Bundle();
                bundle5.putString("txt5", txt5);
                intent.putExtras(bundle5);

                startActivity(intent);
            }
        });
    }

}
