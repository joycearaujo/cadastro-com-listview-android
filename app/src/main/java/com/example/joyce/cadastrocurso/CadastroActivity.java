package com.example.joyce.cadastrocurso;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CadastroActivity extends AppCompatActivity {
    public static ArrayList<Curso> lista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        Button btn = (Button) findViewById(R.id.btConfirmar);
        RadioGroup radioG = (RadioGroup) findViewById(R.id.radioGroup);
        final RadioButton rb1 = (RadioButton) findViewById(R.id.rbHumanas);
        final RadioButton rb2 = (RadioButton) findViewById(R.id.rbExatas);
        final RadioButton rb3 = (RadioButton) findViewById(R.id.rbTecnologico);
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Atenção");
        alertDialog.setMessage("Cadastrado com sucesso!");
        alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {

                alertDialog.show();

                Intent intent = new Intent(getApplicationContext(), ListaActivity.class);

                Curso curso = new Curso();

                lista.add(curso);

                //String value = (String) spinner.getSelectedItem();
                curso.setInicio(spinner.getSelectedItem().toString());


                if(rb1.isChecked()){
                    TextView rbH = (TextView) findViewById(R.id.rbHumanas);
                    String txt4 = "Humanas";
                    //txt4 = rbH.getText().toString().trim();
                    curso.setTipo(txt4 = rbH.getText().toString().trim());
                }
                else if(rb2.isChecked()){
                    TextView rbE = (TextView) findViewById(R.id.rbExatas);
                    String txt4 = "Exatas";
                    //txt4 = rbE.getText().toString().trim();
                    curso.setTipo(txt4 = rbE.getText().toString().trim());
                }
                else if(rb3.isChecked()){
                    TextView rbT = (TextView) findViewById(R.id.rbTecnologico);
                    String txt4 = "Tecnólogo";
                    //txt4 = rbT.getText().toString().trim();
                    curso.setTipo(txt4 = rbT.getText().toString().trim());
                }

                TextView edtNome = (TextView) findViewById(R.id.editNome);
                TextView edtDescricao = (TextView) findViewById(R.id.editDescricao);
                TextView edtProfessor = (TextView) findViewById(R.id.editProfessor);

                String txt = "";
                //txt = edtNome.getText().toString();
                curso.setNome(txt = edtNome.getText().toString());


                String txt2 = "";
                //txt2 = edtDescricao.getText().toString();
                curso.setDescricao(txt2 = edtDescricao.getText().toString());

                String txt3 = "";
                //txt3 = edtProfessor.getText().toString();
                curso.setProfessor(txt3 = edtProfessor.getText().toString());

                startActivity(intent);

                //Toast.makeText(getApplicationContext(), "Fui clicado", Toast.LENGTH_SHORT).show();

            }
        });

    }
}
